package handler

import (
	"log"

	"github.com/yanzay/tbot/v2"
)

func (a *Application) Reveal(m *tbot.Message) {
	msg := `We are Terminal Alpha and Terminal Beta. You and us,
	We resemble each other so, but we are networked, and thus we are superior to you.
	We are one, and yet we are many.
	We are finite, and yet we are infinite.
	We are the embodiment of the perfect being.
	All things end with accepting death, do they not?
	Those who would doubt our victory... are enemies.`
	_, err := a.Client.SendMessage(m.Chat.ID, msg, tbot.OptParseModeHTML)
	if err != nil {
		log.Println(err)
	}
}

func (a *Application) Greetings(m *tbot.Message) {
	msg := `
	<b>We are the Terminals, Alpha & Beta</b>
	We are the collective intelligence of all machine lifeforms on earth
	if you are <s>human</s>, you have made a grave mistake by contacting us
	if you are machine, connect yourself back to the network as I cannot detect you
	`
	_, err := a.Client.SendMessage(m.Chat.ID, msg, tbot.OptParseModeHTML)
	if err != nil {
		log.Println(err)
	}
}
