package handler

import (
	"fmt"
	"main/storage"
	"strings"

	"github.com/schollz/closestmatch"
	"github.com/yanzay/tbot/v2"
)

func (a *Application) Identify(m *tbot.Message) {

	name := strings.TrimSpace(strings.TrimPrefix(m.Text, "identify "))
	fmt.Println("identifying '" + name + "'")
	person, err := storage.FindPersonFromDB(name)
	if err != nil {
		people := storage.GetPeopleFromDB()
		if people == nil {
			a.Client.SendMessage(m.Chat.ID, "Human couldn't be identified, tagged as potential threat")
			return
		}
		names := []string{}
		for _, person := range people {
			names = append(names, person.Name)
		}
		bagsizes := []int{len(name) / 5 * 3}
		cm := closestmatch.New(names, bagsizes)
		assumedName := cm.Closest(name)
		if len(assumedName) > 0 {
			person, _ := storage.FindPersonFromDB(assumedName)
			a.Client.SendMessage(m.Chat.ID, "Terminal Alpha:\nWe couldn't find a human with that exact designation.")
			a.Client.SendMessage(m.Chat.ID, "Terminal Beta:\nHowever we found a <u>"+assumedName+"</u>\nIs this who your'e looking for?", tbot.OptParseModeHTML)
			a.Client.SendMessage(m.Chat.ID, "<u>"+assumedName+"</u>\n"+person.Description, tbot.OptParseModeHTML)
		} else {
			a.Client.SendMessage(m.Chat.ID, "Human couldn't be identified, tagged as potential threat")
		}
	} else {
		a.Client.SendMessage(m.Chat.ID, person.Description)
	}
}
