package handler

import (
	"log"
	"strings"

	"github.com/yanzay/tbot/v2"
	"gopkg.in/jdkato/prose.v2"
)

func (a *Application) Analyzer(m *tbot.Message) {
	text := strings.TrimSpace(strings.TrimPrefix(m.Text, "analyze "))
	log.Println("analyze message: " + text)
	doc, err := prose.NewDocument(text)
	if err != nil {
		log.Fatal(err)
	}
	prepositions := "prepositions: "
	adjectives := "adjectives: "
	verbs := "verbs: "
	nouns := "nouns: "
	others := "others: "
	for _, token := range doc.Tokens() {
		if token.Tag == "NN" {
			nouns = nouns + token.Text + ", "
		} else if token.Tag == "VB" || token.Tag == "VBZ" {
			verbs = verbs + token.Text + ", "
		} else if token.Tag == "PRP" {
			prepositions = prepositions + token.Text + ", "
		} else if token.Tag == "JJ" {
			adjectives = adjectives + token.Text + ", "
		} else {
			others = others + token.Text + " " + token.Tag + ", "
		}
	}
	a.Client.SendMessage(m.Chat.ID, "\n"+prepositions+"\n"+verbs+"\n"+nouns+"\n"+adjectives+"\n"+others)

}
