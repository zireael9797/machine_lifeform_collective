package handler

import (
	"context"
	"fmt"
	"main/widget"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	googlesearch "github.com/rocketlaunchr/google-search"
	"github.com/yanzay/tbot/v2"
)

type Application struct {
	Client *tbot.Client
}

var (
	Handlers Application
	ctx      context.Context
)

func init() {
	fmt.Println("initialized handlers")
	ctx = context.Background()
	UserStates = make(map[int]userStateRecord)
}

func Filter(h tbot.UpdateHandler) tbot.UpdateHandler {
	return func(u *tbot.Update) {
		if u.Message != nil {
			me, err := Handlers.Client.GetMe()
			if err == nil {
				name := me.Username
				if u.Message.Chat.Type != "private" {
					if len(UserStates[u.Message.From.ID].State) > 0 || strings.Contains(u.Message.Text, "@"+name) {
						u.Message.Text = trim(u.Message.Text, "@"+name)
						h(u)
					}

				} else {
					u.Message.Text = trim(u.Message.Text, "@"+name)
					h(u)
				}
			}

		} else {
			h(u)
		}
	}
}
func trim(text string, mention string) string {
	text = strings.ReplaceAll(text, mention, "")
	text = strings.TrimSpace(text)
	space := regexp.MustCompile(`\s+`)
	text = space.ReplaceAllString(text, " ")
	text = strings.TrimPrefix(text, "/")
	text = strings.ToLower(text)
	fmt.Println(text)
	return text
}

//-------------------------Used whenever something is interacting with user states or message history
var deleteWg sync.WaitGroup
var updateWg sync.WaitGroup

const timeMemory = 60

var (
	UserStates map[int]userStateRecord
)

type userState string

type userStateRecord struct {
	Username string
	State    userState
	Last     time.Time
	Chat     string
	History  []string
}

func ClearStateHistory(id int, state userState) {
	//updateWg.Wait()
	fmt.Println("delete request for " + state)
	time.Sleep(time.Second * timeMemory)
	deleteWg.Add(1)
	defer deleteWg.Done()
	fmt.Println("currentstate is " + UserStates[id].State)
	fmt.Println("to delete state is " + state)
	if UserStates[id].State == state {
		//-----------Time check required cause otherwise it may delete a subsequesnt state of the same type
		if UserStates[id].Last.Add(time.Second * timeMemory).Before(time.Now()) {
			Handlers.Client.SendMessage(UserStates[id].Chat, "unit id code: "+UserStates[id].Username+"\nYou have been silent for too long\nWe cannot wait on you any longer")
			delete(UserStates, id)
		}
	}
}

func (a *Application) CallbackHandler(c *tbot.CallbackQuery) {
	fmt.Println("callback")
	a.Client.SendMessage(c.Message.Chat.ID, "Terminal Beta:\nFine, We will attempt to uncover some more relevant information for you")
	fmt.Println("fetching more results for " + c.Data)
	results, err := googlesearch.Search(ctx, c.Data)
	if err == nil {
		fmt.Printf("fetched %d results\n", len(results))
		for i, result := range results {
			markup := widget.MakeUrlButton("Details", result.URL)
			a.Client.SendMessage(c.Message.Chat.ID, strconv.Itoa(i+1)+"\n"+result.Description, tbot.OptInlineKeyboardMarkup(&markup))
		}
		a.Client.SendMessage(c.Message.Chat.ID, "Terminal Alpha:\nWe have recovered these documents from the preserved archives\nhuman lifestyle is quite amusing, absorbed in vanity and aimless idling")
	}
}

func (a *Application) All(m *tbot.Message) {
	user := m.From
	deleteWg.Wait()
	fmt.Println("All: " + UserStates[user.ID].State)
	if len(UserStates[user.ID].State) <= 0 {
		msg := m.Text
		if strings.HasPrefix(msg, "who are you") {
			a.Reveal(m)
		} else if strings.HasPrefix(msg, "chat ") {
			a.Chat(m)
		} else if msg == "start" {
			a.Greetings(m)
		} else if msg == "greet" {
			a.Greetings(m)
		} else if strings.HasPrefix(msg, "identify ") {
			a.Identify(m)
		} else if strings.HasPrefix(msg, "search ") || msg == "search" {
			a.Search(m)
		} else if msg == "test" {
			a.Test(m)
		} else if strings.HasPrefix(msg, "analyze ") {
			a.Analyzer(m)
		} else if msg == "corona" {
			a.Corona(m)
		} else {
			msg := `I don't know how to respond to that
		you can do '/start'
		you can do '/greet'
		you can do '/chat' <anything>
		you can do '/identify <name>'
		you can do '/search <searchterm>'
		you can do '/analyze <anything>'
		you can do '/corona'`
			a.Client.SendMessage(m.Chat.ID, msg)
		}
	} else if UserStates[user.ID].State == "chatting" {
		a.ContinueChat(m)
	} else if UserStates[user.ID].State == "searching" {
		a.ContinueSearch(m, strings.TrimPrefix(m.Text, "/"))
	} else {
		a.Client.SendMessage(m.Chat.ID, "We were apparently doing something? but I don't remember what.")
	}
}
