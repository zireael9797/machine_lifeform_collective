package handler

import (
	"fmt"
	"main/widget"
	"strings"
	"time"

	googlesearch "github.com/rocketlaunchr/google-search"
	"github.com/yanzay/tbot/v2"
)

func (a *Application) Search(m *tbot.Message) {

	search := strings.TrimSpace(strings.TrimPrefix(m.Text, "search"))
	fmt.Println(search)
	// updateWg.Add(1)
	// defer updateWg.Done()
	if search == "" {
		user := m.From
		var name string
		if len(user.Username) > 0 {
			name = user.Username
		} else {
			name = user.FirstName
		}
		UserStates[user.ID] = userStateRecord{
			Username: name,        //--------SET1
			State:    "searching", //--------SET2
			Last:     time.Now(),  //--------SET3
			Chat:     m.Chat.ID,   //--------SET4
		} //--------UPDATE

		go ClearStateHistory(user.ID, "searching")
		a.Client.SendMessage(m.Chat.ID, "<b>Terminal Alpha:</b> Don't keep us waiting, tell us your search term", tbot.OptParseModeHTML)
	} else {
		a.ContinueSearch(m, strings.TrimSpace(search))
	}
}

func (a *Application) ContinueSearch(m *tbot.Message, search string) {
	user := m.From
	// updateWg.Wait()
	fmt.Println("fetching results for " + search)
	delete(UserStates, user.ID)
	results, err := googlesearch.Search(ctx, search, googlesearch.SearchOptions{Limit: 3})
	if err == nil {
		fmt.Printf("fetched %d results\n", len(results))
		for _, result := range results {
			markup := widget.MakeUrlButton("Details", result.URL)
			a.Client.SendMessage(m.Chat.ID, result.Description, tbot.OptInlineKeyboardMarkup(&markup))
		}
		loadmoremarkup := widget.MakeLoadMoreButton("Load More", search)
		a.Client.SendMessage(m.Chat.ID, "<b>Terminal Beta:</b> We have found these texts from the primitive knowledge archive of the human era known as the internet", tbot.OptParseModeHTML, tbot.OptInlineKeyboardMarkup(&loadmoremarkup))
	}
}
