package handler

import (
	"strings"
	"time"

	"github.com/yanzay/tbot/v2"
)

const messageMemory = 5

func (a *Application) Chat(m *tbot.Message) {

	user := m.From
	// updateWg.Add(1)
	// defer updateWg.Done()
	var userState userStateRecord //-------CREATE
	userState.History = append(userState.History, strings.TrimPrefix(m.Text, "chat "))

	if len(user.Username) > 0 {
		userState.Username = user.Username
	} else {
		userState.Username = user.FirstName //------- SET1
	}
	userState.State = "chatting" //-------SET2
	userState.Chat = m.Chat.ID   //-------SET3
	userState.Last = time.Now()  //-------SET4
	deleteWg.Wait()
	a.Client.SendMessage(m.Chat.ID, "unit id code: "+userState.Username+"\nyou have our attention for a while\nyour first query is\n<u><b>"+userState.History[0]+"</b></u>\nwe will listen to your followup queries", tbot.OptParseModeHTML)
	UserStates[user.ID] = userState //-------UPDATE
	go ClearStateHistory(user.ID, "chatting")

}
func (a *Application) ContinueChat(m *tbot.Message) {
	user := m.From
	// updateWg.Add(1)
	// defer updateWg.Done()
	deleteWg.Wait()
	state := UserStates[user.ID] //-------LOAD
	state.History = append(state.History, strings.TrimPrefix(m.Text, "/"))
	length := len(state.History)
	if length > messageMemory {
		state.History = state.History[length-messageMemory:] //------- SET5
	}
	state.Chat = m.Chat.ID      //------- SET1
	if len(user.Username) > 0 { //------- SET2
		state.Username = user.Username
	} else {
		state.Username = user.FirstName
	}
	state.Last = time.Now()  //------- SET3
	state.State = "chatting" //------- SET4
	deleteWg.Wait()
	UserStates[user.ID] = state //------- UPDATE
	go ClearStateHistory(user.ID, "chatting")
	messages := ""
	for _, message := range state.History {
		messages = messages + "\n" + message
	}
	a.Client.SendMessage(m.Chat.ID, "unit id code: "+state.Username+"\nyour queries are<u><b>"+messages+"</b></u>", tbot.OptParseModeHTML)

}
