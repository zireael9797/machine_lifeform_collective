package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/bradfitz/slice"
	"github.com/yanzay/tbot/v2"
)

type Country struct {
	Country        string `json:"Country"`
	TotalConfirmed int    `json:"TotalConfirmed"`
	TotalDeaths    int    `json:"TotalDeaths"`
	NewConfirmed   int    `json:"NewConfirmed"`
	NewDeaths      int    `json:"NewDeaths"`
}
type Countries struct {
	Countries []Country `json:"Countries"`
	Date      time.Time `json:"Date"`
}

func (a *Application) Corona(m *tbot.Message) {
	a.Client.SendMessage(m.Chat.ID, "Checking for corona epidemic records on this day of the first year")
	resp, err := http.Get("https://api.covid19api.com/summary")
	fmt.Println("request complete")
	if err != nil {
		log.Panic(err)
		a.Client.SendMessage(m.Chat.ID, "Couldn't aquire the requested info on the Corona epidemic")
		return
	}
	if resp.StatusCode != http.StatusOK {
		a.Client.SendMessage(m.Chat.ID, "Couldn't aquire the requested info on the Corona epidemic")
		return
	}
	defer resp.Body.Close()
	var countries Countries
	err = json.NewDecoder(resp.Body).Decode(&countries)
	if err != nil {
		fmt.Println(err)
		a.Client.SendMessage(m.Chat.ID, "Couldn't aquire the requested info on the Corona epidemic")
		return
	}
	messages := "<u>Top Total Cases</u>\n"
	topCountries := countries.Countries
	slice.Sort(topCountries[:], func(i, j int) bool {
		return topCountries[i].TotalConfirmed > topCountries[j].TotalConfirmed
	})
	topCountries = topCountries[:10]
	for _, country := range topCountries {
		messages += fmt.Sprintf("\n<b>%s</b>", (country.Country))
		messages += fmt.Sprintf("\nConfirmed: %d", (country.TotalConfirmed))
		messages += fmt.Sprintf("\nDeaths:    %d", (country.TotalDeaths))
		messages += "\n"
	}
	newCountries := countries.Countries
	slice.Sort(newCountries[:], func(i, j int) bool {
		return newCountries[i].NewConfirmed > newCountries[j].NewConfirmed
	})
	a.Client.SendMessage(m.Chat.ID, messages, tbot.OptParseModeHTML)
	messages = "\n<u>Top New Cases</u>\n"
	newCountries = newCountries[:10]
	for _, country := range topCountries {
		messages += fmt.Sprintf("\n<b>%s</b>", (country.Country))
		messages += fmt.Sprintf("\nNew Confirmed: %d", (country.NewConfirmed))
		messages += fmt.Sprintf("\nNew Deaths:    %d", (country.NewDeaths))
		messages += "\n"
	}
	totalInfected := 0
	totalDead := 0
	for _, country := range countries.Countries {
		totalInfected += country.TotalConfirmed
		totalDead += country.TotalDeaths
	}
	a.Client.SendMessage(m.Chat.ID, messages, tbot.OptParseModeHTML)
	a.Client.SendMessage(m.Chat.ID, fmt.Sprintf("%d humans <b>infected</b>\n%d humans <b>neutralized</b>", totalInfected, totalDead), tbot.OptParseModeHTML)
	a.Client.SendMessage(m.Chat.ID, "The virus seemed to be performing better than our initial tests\nPresumably because of sheer apathy on the humans' part")
}

func (a *Application) SpellVaiya(m *tbot.Message) {
	a.Client.SendMessage(m.Chat.ID, "<b>Terminal Alpha:</b> It's obviously <b><u>VAIYA</u></b>\n<b>Terminal Beta:</b> even idiots know that", tbot.OptParseModeHTML)
}
