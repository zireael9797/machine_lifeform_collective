package main

import (
	"log"
	"main/handler"
	"os"

	"github.com/joho/godotenv"
	"github.com/yanzay/tbot/v2"
)

var (
	terminal_alpha_beta *tbot.Server
	token               string
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Println(err)
	}
	token = os.Getenv("TELEGRAM_TOKEN")
}

func main() {
	terminal_alpha_beta = tbot.New(token)
	handler.Handlers.Client = terminal_alpha_beta.Client()
	terminal_alpha_beta.Use(handler.Filter)

	terminal_alpha_beta.HandleMessage(".", handler.Handlers.All)
	// terminal_alpha_beta.HandleMessage("^who are you?", handler.Handlers.Reveal)
	// terminal_alpha_beta.HandleMessage("^is it bhaiya or vaiya|is it vaiya or bhaiya", handler.Handlers.SpellVaiya)
	// terminal_alpha_beta.HandleMessage("^chat ", handler.Handlers.Chat)
	// terminal_alpha_beta.HandleMessage("^start$", handler.Handlers.Greetings)
	// terminal_alpha_beta.HandleMessage("^greet$", handler.Handlers.Greetings)
	// terminal_alpha_beta.HandleMessage("^identify ", handler.Handlers.Identify)
	// terminal_alpha_beta.HandleMessage("^search", handler.Handlers.Search)
	// terminal_alpha_beta.HandleMessage("^test$", handler.Handlers.Test)
	// terminal_alpha_beta.HandleMessage("^analyze ", handler.Handlers.Analyzer)
	// terminal_alpha_beta.HandleMessage("^corona$", handler.Handlers.Corona)
	terminal_alpha_beta.HandleCallback(handler.Handlers.CallbackHandler)
	log.Println("Terminal Online\nTerminal Alpha: Is this a situation where one would laugh?\nTerminal Beta: HAhAhAhaHaHAhA")
	log.Fatal(terminal_alpha_beta.Start())
}
