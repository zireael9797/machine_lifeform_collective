module main

go 1.14

require (
	cloud.google.com/go/firestore v1.2.0
	github.com/bradfitz/slice v0.0.0-20180809154707-2b758aa73013
	github.com/deckarep/golang-set v1.7.1 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/mingrammer/commonregex v1.0.1 // indirect
	github.com/rocketlaunchr/google-search v0.0.0-20200311231904-72db8d8504cb
	github.com/schollz/closestmatch v2.1.0+incompatible
	github.com/yanzay/tbot/v2 v2.1.0
	gonum.org/v1/gonum v0.7.0 // indirect
	google.golang.org/api v0.20.0
	gopkg.in/jdkato/prose.v2 v2.0.0-20190814032740-822d591a158c
	gopkg.in/neurosnap/sentences.v1 v1.0.6 // indirect
)
