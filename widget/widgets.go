package widget

import "github.com/yanzay/tbot/v2"

func MakeUrlButton(text string, url string) tbot.InlineKeyboardMarkup {
	markup := tbot.InlineKeyboardMarkup{
		InlineKeyboard: [][]tbot.InlineKeyboardButton{
			[]tbot.InlineKeyboardButton{
				tbot.InlineKeyboardButton{
					Text: text,
					URL:  url,
				},
			},
		},
	}
	return markup
}

func MakeLoadMoreButton(text string, url string) tbot.InlineKeyboardMarkup {
	markup := tbot.InlineKeyboardMarkup{
		InlineKeyboard: [][]tbot.InlineKeyboardButton{
			[]tbot.InlineKeyboardButton{
				tbot.InlineKeyboardButton{
					Text:         text,
					CallbackData: url,
				},
			},
		},
	}
	return markup
}
